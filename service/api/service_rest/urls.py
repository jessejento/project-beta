from django.urls import path
from .views import api_technicians, api_technician, api_appointment
from .views import api_appointments, finish_appointment, cancel_appointment

urlpatterns = [
 path('technicians/', api_technicians, name='api_technicians'),
 path('technicians/<int:pk>/', api_technician, name='api_technician'),
 path('appointments/', api_appointment, name='api_appointment'),
 path('appointments/<int:pk>/', api_appointments, name='api_appointments'),
 path('appointments/<int:pk>/canceled', cancel_appointment,
      name='cancel_appointment'),
 path('appointments/<int:pk>/finished', finish_appointment,
      name='finish_appointment')
]
