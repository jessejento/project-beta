import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Technician, Appointment
from .encoders import TechnicianEncoder, AppointmentEncoder


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_appointment(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": list(appointments)},
            encoder=AppointmentEncoder,
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician_id = content.get("technician")
            try:
                technician = Technician.objects.get(id=technician_id)
            except Technician.DoesNotExist:
                response = JsonResponse(
                    {"message": "Technician does not exist"},
                    status=400
                )
                return response
            appointment = Appointment.objects.create(
                date_time=content["date_time"],
                reason=content["reason"],
                status=content["status"],
                vin=content["vin"],
                customer=content["customer"],
                technician=technician,
            )

            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Exception as e:
            response = JsonResponse(
                {"message": "Failed to create an appointment: {}".format(str
                 (e))},
                status=400
            )
            return response
    else:
        response = JsonResponse(
            {"message": "Method not allowed"},
            status=405
        )
        return response


@require_http_methods(["DELETE", "GET"])
def api_appointments(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message":
                                     " Appopointment Does not exist"})
            response.status_code = 400
            return response
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["PUT"])
def finish_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
    except Appointment.DoesNotExist:
        return JsonResponse({"error": "Appointment not found."}, status=404)
    appointment.status = "finished"
    appointment.save()
    return JsonResponse(
        {"message": "Appointment finished successfully."},
        status=200,
    )


@require_http_methods(["PUT"])
def cancel_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
    except Appointment.DoesNotExist:
        return JsonResponse({"error": "Appointment not found."}, status=404)
    appointment.status = "canceled"
    appointment.save()
    return JsonResponse(
        {"message": "Appointment canceled successfully."},
        status=200,
    )
