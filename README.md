# CarCar

Team:

* Jesse Jento - Sales Microservice
* Isaac Henderson - Service Microservice

## Design
Backend in order:
Models-Poller-Views-Urls

Frontend in order:
Inventory(manufacturer, model, automobile)
Sales (customer, salesperson)/Service(technician)
Sales (sales)/Service(appointment)


## Service microservice

First, there's the AutomobileVO model, which keeps track of  cars in the system. Each car has a unique identifier called (VIN), and we also know whether it has been sold or not.
Next, we have the Technician model, which represents the skilled professionals working in our service. Each technician has a first name, a last name, and a unique employee ID that distinguishes them from others.
Finally, we have the Appointment model, which is the heart of our system. It captures all the appointment details, such as the date and time, the reason for the appointment, and its status. Additionally, each appointment is associated with a specific car (identified by its VIN) and a technician.
By using these models, our microservice can efficiently manage and organize data related to cars, technicians, and appointments, providing a robust and user-friendly appointment management system.
In our front-end system, we have specialized forms for technicians and appointments. These forms enable us to interact with the back-end services by sending data through HTTP requests like POST and GET. With the technician form, we can add new technician details and view existing ones. Similarly, the appointment form allows us to create new appointments, retrieve appointment information, and make changes when needed. These forms serve as the gateway for communication between the front-end and back-end components of our system, providing a streamlined way to manage technicians and appointments seamlessly.
## Sales microservice

My models are AutomobileVO, Salesperson, Customer and Sale.

Salesperson and Customer are for building a sale.

The AutmobileVO is used for detecting the Vins of the autmobiles created in the inventory.

A sale requires a Salesperson ID, Customer ID and AutomobileVO VIN as forign keys to be created.

When creating a sale on the frontend, it wont display an automobile if it is already sold; if it did somehow display an already sold vin and you tried to create a new sale with it, create sale in my views won't let it create a sale with an already sold car.

When a sale is created it sends a PUT request to io the inventory endpoint for the automobile VIN it used and changes the sold to true to display on the list automobiles and so you can no longer create a sale with that car.
