import React, { useState, useEffect } from 'react';

function CustomerPage({ name, address, phone_number, onDelete }) {
  const handleDelete = () => {
    const confirmDelete = window.confirm('Are you sure you want to delete this customer?');
    if (confirmDelete) {
      onDelete();
    }
  };

  return (
    <tr>
      <td>{name}</td>
      <td>{address}</td>
      <td>{phone_number}</td>
      <td>
        <button onClick={handleDelete}>Delete</button>
      </td>
    </tr>
  );
}

function CustomerPageContainer() {
  const [customersData, setCustomersData] = useState([]);

  useEffect(() => {
    const fetchCustomerData = async () => {
      const url = 'http://localhost:8090/api/customers/';
      try {
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error('Bad response!');
        } else {
          const data = await response.json();
          const customers = data.customers.map((customer) => ({
            id: customer.id,
            name: customer.first_name + " " + customer.last_name,
            address: customer.address,
            phone_number: customer.phone_number,
          }));

          setCustomersData(customers);
        }
      } catch (e) {
        console.error(e);
        alert('Error was raised!');
      }
    };

    fetchCustomerData();
  }, []);

  const handleDeleteCustomer = async (customerIndex) => {
    const customerToDelete = customersData[customerIndex];
    const deleteUrl = `http://localhost:8090/api/customers/${customerToDelete.id}`;
    try {
      const response = await fetch(deleteUrl, {
        method: 'DELETE',
      });

      if (response.ok) {
        setCustomersData((prevCustomers) => prevCustomers.filter((_, index) => index !== customerIndex));
      } else {
        throw new Error('Error occurred while deleting the customer!');
      }
    } catch (e) {
      console.error(e);
      alert('Error occurred while deleting the customer!');
    }
  };

  return (
    <div>
      <table id="customerContainer" className="table table-striped">
        <thead>
          <h1>Customers</h1>
          <tr>
            <th>Name</th>
            <th>Address</th>
            <th>Phone Number</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {customersData.map((customer, index) => (
            <CustomerPage
              key={index}
              name={customer.name}
              address={customer.address}
              phone_number={customer.phone_number}
              onDelete={() => handleDeleteCustomer(index)}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default CustomerPageContainer;
