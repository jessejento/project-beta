import React, { useState, useEffect } from 'react';

function AutomobilePage({ color, year, model, vin, sold, manufacturer })
{
  const soldDisplayValue = sold ? 'Yes' : 'No';

  return (
    <tr>
      <td>{vin}</td>
      <td>{color}</td>
      <td>{year}</td>
      <td>{model}</td>
      <td>{manufacturer}</td>
      <td>{soldDisplayValue}</td>
    </tr>
  );
  }
function AutomobilePageContainer() {
  const [automobilesData, setAutomobilesData] = useState([]);

  useEffect(() => {
    const fetchAutomobileData = async () => {
      const url = 'http://localhost:8100/api/automobiles/';
      try {
        const response = await fetch(url);

        if (!response.ok) {
          throw new Error('Bad response!');
        } else {
          const data = await response.json();
          const automobiles = data.autos.map((automobile) => ({
            id: automobile.id,
            color: automobile.color,
            year: automobile.year,
            vin: automobile.vin,
            model: automobile.model.name,
            manufacturer: automobile.model.manufacturer.name,
            sold: automobile.sold,
          }));

          setAutomobilesData(automobiles);
        }
      } catch (e) {
        console.error(e);
        alert('Error was raised!');
      }
    };

    fetchAutomobileData();
  }, []);

  return (
    <div>
      <table id="automobileContainer" className="table table-striped">
        <thead>
          <h1>Automobiles</h1>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {automobilesData.map((automobile, index) => (
            <AutomobilePage
              key={index}
              color={automobile.color}
              year={automobile.year}
              vin={automobile.vin}
              model={automobile.model}
              manufacturer={automobile.manufacturer}
              sold={automobile.sold}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AutomobilePageContainer;
