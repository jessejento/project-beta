function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">Lux</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
         
"Elevating Elegance, Redefining Luxury: Your Journey to Exquisite Driving."
        </p>
      </div>
    </div>
  );
}

export default MainPage;
