import React, { useState, useEffect } from 'react';

function SalespersonPage({ name, employee_id, onDelete }) {
  const handleDelete = () => {
    const confirmDelete = window.confirm('Are you sure you want to delete this salesperson?');
    if (confirmDelete) {
      onDelete();
    }
  };

  return (
    <tr>
      <td>{name}</td>
      <td>{employee_id}</td>
      <td>
        <button onClick={handleDelete}>Delete</button>
      </td>
    </tr>
  );
}

function SalespersonPageContainer() {
  const [salespeopleData, setSalespersonsData] = useState([]);

  useEffect(() => {
    const fetchSalespersonData = async () => {
      const url = 'http://localhost:8090/api/salespeople/';
      try {
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error('Bad response!');
        } else {
          const data = await response.json();
          const salespeople = data.salespeople.map((salesperson) => ({
            id: salesperson.id,
            first_name: salesperson.first_name,
            last_name: salesperson.last_name,
            employee_id: salesperson.employee_id,
          }));

          setSalespersonsData(salespeople);
        }
      } catch (e) {
        console.error(e);
        alert('Error was raised!');
      }
    };

    fetchSalespersonData();
  }, []);

  const handleDeleteSalesperson = async (salespersonIndex) => {
    const salespersonToDelete = salespeopleData[salespersonIndex];
    const deleteUrl = `http://localhost:8090/api/salespeople/${salespersonToDelete.id}`;
    try {
      const response = await fetch(deleteUrl, {
        method: 'DELETE',
      });

      if (response.ok) {
        setSalespersonsData((prevSalespersons) => prevSalespersons.filter((_, index) => index !== salespersonIndex));
      } else {
        throw new Error('Error occurred while deleting the salesperson!');
      }
    } catch (e) {
      console.error(e);
      alert('Error occurred while deleting the salesperson!');
    }
  };

  return (
    <div>
      <table id="salespersonContainer" className="table table-striped">
        <thead>
          <h1>Salesperson</h1>
          <tr>
            <th>Name</th>
            <th>Employee ID</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {salespeopleData.map((salesperson, index) => (
            <SalespersonPage
              key={index}
              name={salesperson.first_name + " " + salesperson.last_name}
              employee_id={salesperson.employee_id}
              onDelete={() => handleDeleteSalesperson(index)}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalespersonPageContainer;
