import React, { useEffect, useState } from 'react';

function VehicleModelForm() {
  const [formData, setFormData] = useState({
    name: '',
    picture_url: '',
    manufacturer_id: '',
  });

  const [manufacturers, setManufacturers] = useState([]);
  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setFormData({
          name: '',
          picture_url: '',
          manufacturer_id: '',
        });
        alert("Vehicle Model created successfully!");
      } else {
        alert("Failed to add the Vehicle Model!");
      }
    } catch (e) {
      console.error("An error occurred during the fetch operation:", e);
      alert("Error occurred!");
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  }
  useEffect(() => {
    async function fetchManufacturers() {
      try {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (!response.ok) {
          alert("Bad response while fetching manufacturers!");
        } else {
          const data = await response.json();
          setManufacturers(data.manufacturers);
        }
      } catch (e) {
        console.error("An error occurred during the fetch operation:", e);
        alert("Error occurred while fetching manufacturers!");
      }
    }

    fetchManufacturers();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a vehicle model</h1>
          <form onSubmit={handleSubmit} id="add-hat-form">

          <div className="form-floating mb-4">
              <input onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={formData.name} />
              <label htmlFor="name">Model Name</label>
            </div>

            <div className="form-floating">
              <input onChange={handleFormChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" value={formData.picture_url} />
              <label htmlFor="pictureUrl">Picture URL</label>
            </div>

            <div className="mb-3">
              <label htmlFor="manufacturer_id"></label>
              <select onChange={handleFormChange} name="manufacturer_id" id="manufacturer_id" className="form-select" value={formData.manufacturer_id}>
                <option value="">Choose a manufacturer</option>
                {manufacturers.map(manufacturer_id => {
                  return (
                    <option key={manufacturer_id.id} value={manufacturer_id.id}>{manufacturer_id.name}</option>
                  )
                })}
              </select>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default VehicleModelForm;
