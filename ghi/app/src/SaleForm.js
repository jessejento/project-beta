import React, { useEffect, useState } from 'react';

function SaleForm() {
  const [formData, setFormData] = useState({
    automobile: '',
    salesperson: '',
    customer: '',
    price: '',
  });

  const [automobiles, setAutomobiles] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const handleSubmit = async (event) => {
    event.preventDefault();


    const putData = {sold: true}
    const putUrl = `http://localhost:8100/api/automobiles/${formData.automobile}/`;
    const putFetchConfig = {
      method: "put",
      body: JSON.stringify(putData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const url = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setFormData({
          automobile: '',
          salesperson: '',
          customer: '',
          price: '',
        });
        const putResponse = await fetch(putUrl, putFetchConfig);
        if (putResponse.ok) {
        alert("Sold!")

        }
      } else {
        alert("Failed to add the Sale!");
      }
    } catch (e) {
      console.error("An error occurred during the fetch operation:", e);
      alert("Error occurred!");
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  useEffect(() => {


    async function fetchAutomobiles() {
      try {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (!response.ok) {
          alert("Bad response while fetching automobiles!");
        } else {
          const data = await response.json();
          const filteredAutomobiles = data.autos.filter(automobile => !automobile.sold);
          setAutomobiles(filteredAutomobiles);
        }
      } catch (e) {
        console.error("An error occurred during the fetch operation:", e);
        alert("Error occurred while fetching automobiles!");
      }
    }


    async function fetchCustomers() {
      try {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (!response.ok) {
          alert("Bad response while fetching customers!");
        } else {
          const data = await response.json();
          setCustomers(data.customers);
        }
      } catch (e) {
        console.error("An error occurred during the fetch operation:", e);
        alert("Error occurred while fetching customers!");
      }
    }

    async function fetchSalespeople() {
      try {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (!response.ok) {
          alert("Bad response while fetching salespeople!");
        } else {
          const data = await response.json();
          setSalespeople(data.salespeople);
        }
      } catch (e) {
        console.error("An error occurred during the fetch operation:", e);
        alert("Error occurred while fetching salespeople!");
      }
    }

    fetchAutomobiles();
    fetchCustomers();
    fetchSalespeople();
  }, []);
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a sale</h1>
          <form onSubmit={handleSubmit} id="add-sale-form">

            <div className="mb-3">
              <label htmlFor="automobile"></label>
              <select onChange={handleFormChange} name="automobile" id="automobile" className="form-select" value={formData.automobile}>
                <option value="">Choose an automobile VIN</option>
                {automobiles.map(automobile => {
                  return (
                    <option key={automobile.id} value={automobile.vin}>{automobile.vin}</option>
                  );
                })}
              </select>
            </div>

            <div className="mb-3">
              <label htmlFor="customer"></label>
              <select onChange={handleFormChange} name="customer" id="customer" className="form-select" value={formData.customer}>
                <option value="">Choose a customer</option>
                {customers.map(customer => {
                  return (
                    <option key={customer.id} value={customer.id}>{customer.first_name}</option>
                  );
                })}
              </select>
            </div>

            <div className="mb-3">
              <label htmlFor="salesperson"></label>
              <select onChange={handleFormChange} name="salesperson" id="salesperson" className="form-select" value={formData.salesperson}>
                <option value="">Choose a salesperson</option>
                {salespeople.map(salesperson => {
                  return (
                    <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name}</option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Price" required type="number" name="price" id="price" className="form-control" value={formData.price} />
            <label htmlFor="year">Price</label>
          </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default SaleForm;
