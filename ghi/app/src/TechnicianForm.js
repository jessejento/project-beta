import React, { useState} from 'react';

function TechnicianForm() {
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    employee_id:'',
  });

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8080/api/technicians/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setFormData({
          first_name: '',
          last_name: '',
          employee_id:'',
        });
        alert("Technician created successfully!");
      } else {
        alert("Failed to add the technician!");
      }
    } catch (e) {
      console.error("An error occurred during the fetch operation:", e);
      alert("Error occurred!");
    }
  }
  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  }


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a technician</h1>
          <form onSubmit={handleSubmit} id="add-technician-form">

          <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" value={formData.name} />
              <label htmlFor="name">First Name </label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" value={formData.name} />
              <label htmlFor="name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control" value={formData.name} />
              <label htmlFor="name">Employee ID</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default TechnicianForm;