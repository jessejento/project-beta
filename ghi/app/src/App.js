import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerPage from './ManufacturerPage';
import VehicleModelPage from './VehicleModelPage';
import AutomobilePage from './AutomobilePage';
import AutomobileForm from './AutomobileForm';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelForm from './VehicleModelForm';
import TechnicianForm from './TechnicianForm';
import TechnicianPage from './TechnicianPage';
import AppointmentForm from './AppointmentForm';
import SalepersonPage from './SalespersonPage';
import SalespersonForm from './SalespersonForm';
import CustomerPage from './CustomerPage';
import CustomerForm from './CustomerForm';
import SalePage from './SalePage';
import SaleForm from './SaleForm';
import AppointmentPage from './AppointmentPage'; 
import SalesHistory from './SalesHistory';
import './App.css';





function App() {
  return (
    <BrowserRouter>
    <div className="bg">
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers" element={<ManufacturerPage />} />
          <Route path="/vehiclemodels" element={<VehicleModelPage />} />
          <Route path="/automobiles" element={<AutomobilePage />} />
          <Route path="/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="/vehiclemodels/new" element={<VehicleModelForm />} />
          <Route path="/automobiles/new" element={<AutomobileForm />} />
          <Route path="/technicians/new" element={<TechnicianForm />} />
          <Route path="/technicians" element={<TechnicianPage />} />
          <Route path="/appointment/new" element={<AppointmentForm />} />
          <Route path="/salespeople" element={<SalepersonPage />} />
          <Route path="/salespeople/new" element={<SalespersonForm />} />
          <Route path="/customers" element={<CustomerPage />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          <Route path="/sales" element={<SalePage />} />
          <Route path="/sales/new" element={<SaleForm />} />
          <Route path="/appointments" element={<AppointmentPage />} />
          <Route path="/saleshistory" element={<SalesHistory />} />
        </Routes>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
