import React, { useState, useEffect } from 'react';

function SalespersonDropdown({ onSelectSalesperson }) {
  const [salespeople, setSalespeople] = useState([]);

  useEffect(() => {
    const fetchSalespeople = async () => {
      const url = 'http://localhost:8090/api/salespeople/';
      try {
        const response = await fetch(url);

        if (!response.ok) {
          throw new Error('Bad response!');
        } else {
          const data = await response.json();
          setSalespeople(data.salespeople);
        }
      } catch (e) {
        console.error(e);
        alert('Error was raised while fetching salespeople!');
      }
    };

    fetchSalespeople();
  }, []);

  const handleSalespersonChange = (event) => {
    const selectedSalesperson = event.target.value;
    onSelectSalesperson(selectedSalesperson);
  };
  return (
    <>
      <h1>Salesperson History</h1>
      <select onChange={handleSalespersonChange}>
        <option value="">Select Salesperson</option>
        {salespeople.map((person) => (
          <option key={person.id} value={person.id}>
            {person.first_name + ' ' + person.last_name}
          </option>
        ))}
      </select>
    </>
  );
}

function SalesHistory({ onDelete, employee_id, salesperson, customer, vin, price }) {
  const handleDelete = () => {
    const confirmDelete = window.confirm('Are you sure you want to delete this sale?');
    if (confirmDelete) {
      onDelete();
    }
  };

  return (
    <tr>
      <td>{employee_id}</td>
      <td>{salesperson}</td>
      <td>{customer}</td>
      <td>{vin}</td>
      <td>{price}</td>
      <td>
        <button onClick={handleDelete}>Delete</button>
      </td>
    </tr>
  );
}

function SalesHistoryContainer() {
  const [salesData, setSalesData] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState('');

  useEffect(() => {
    const fetchSaleData = async () => {
      const url = 'http://localhost:8090/api/sales/';
      try {
        const response = await fetch(url);

        if (!response.ok) {
          throw new Error('Bad response!');
        } else {
          const data = await response.json();
          const sales = data.sales.map((sale) => ({
            id: sale.id,
            employee_id: sale.salesperson.employee_id,
            salesperson: sale.salesperson.first_name + ' ' + sale.salesperson.last_name,
            customer: sale.customer.first_name + ' ' + sale.customer.last_name,
            vin: sale.automobile.vin,
            price: sale.price,
            salesperson_id: sale.salesperson.id,
          }));

          setSalesData(sales);
        }
      } catch (e) {
        console.error(e);
        alert('Error was raised!');
      }
    };

    fetchSaleData();
  }, []);

  const filteredSalesData = selectedSalesperson
    ? salesData.filter((sale) => sale.salesperson_id === selectedSalesperson)
    : salesData;

    const handleDeleteSale = async (saleIndex) => {
      const saleToDelete = salesData[saleIndex];
      const deleteUrl = `http://localhost:8090/api/sales/${saleToDelete.id}`;
      try {
        const response = await fetch(deleteUrl, {
          method: 'DELETE',
        });

        if (response.ok) {
          setSalesData((prevSales) => prevSales.filter((_, index) => index !== saleIndex));
        } else {
          throw new Error('Error occurred while deleting the sale!');
        }
      } catch (e) {
        console.error(e);
        alert('Error occurred while deleting the sale!');
      }
    };
  return (
    <div>
      <SalespersonDropdown onSelectSalesperson={setSelectedSalesperson} />
      <table id="saleContainer" className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson EmployeeID</th>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {filteredSalesData.map((sale, index) => (
            <SalesHistory
              key={index}
              employee_id={sale.employee_id}
              salesperson={sale.salesperson}
              customer={sale.customer}
              vin={sale.vin}
              price={sale.price}
              onDelete={() => handleDeleteSale(index)}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalesHistoryContainer;
